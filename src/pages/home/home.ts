import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LeonisaScanProvider } from '../../providers/leonisa-scan/leonisa-scan';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, 
    public lScan: LeonisaScanProvider 
    ) {

  }

  testAdd(a, b){
    let data = {
      param1: a,
      param2: b
    }

    this.lScan.add(data).then( result =>{
      alert( "Total is: " + result)
    }, err=>{
      alert("Err 1: " + err);
    }).catch(err=>{
      alert("Err 2: " + err);
    })
  }

  testSubstract(a, b){
    let data = {
      param1: a,
      param2: b
    }

    this.lScan.substract(data).then( result =>{
      alert( "Total is: " + result)
    }, err=>{
      alert("Err 1: " + err);
    }).catch(err=>{
      alert("Err 2: " + err);
    })
  }

}
