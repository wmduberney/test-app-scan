import { Injectable } from '@angular/core';
import { Plugin, Cordova, CordovaProperty,  CordovaInstance, IonicNativePlugin } from '@ionic-native/core';

@Plugin(
  {
    pluginName: "leonisascan",
    plugin: "cordova-plugin-leonisascan",
    pluginRef: "LeonisaScan",
    repo: "../leonisaScan",
    platforms: [ "Android", "iOS" ]
  }
)

@Injectable()
export class LeonisaScanProvider {
  @Cordova()  
  add(arg1: any): Promise<string>
  {
    return;
  }

  @Cordova()  
  substract(arg1: any): Promise<string>
  {
    return;
  }

}
